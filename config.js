
// gitName: {  项目git名称 
//     url: 'https://gitlab.com/287207951/gittest2.git', 项目git地址
//     master: 0,   // master分支 对应versions中的第0个版本号
//     ykj: 1       // ykj分支 对应versions中的第1个版本号
// },

const configVersions = {
    gittest2: {
        url: 'https://gitlab.com/287207951/gittest2.git',
        master: 0,   // 对应versions中的第1个版本号
        ykj: 1
    },
    // OMS
    'manage-client': {
        url: 'http://192.168.16.20/batar-sjy/manage-client.git',
        master: 2,   // 对应versions中的第1个版本号
        'V3.8.x.191023': 3
    },
    // BMS
    'stock-manage-client': {
        url: 'http://192.168.16.20/sjy-bms/stock-manage-client.git',
        master: 4,   // 对应versions中的第1个版本号
        'V3.8.x.191023': 5
    },
}

module.exports = configVersions;