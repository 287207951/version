var shell = require("shelljs");
var fs = require("fs");
var argv = require('yargs').argv;
// console.log(argv._[0])
var commitVal = argv._[0] || `version+1`
var edit = require('./edit')
var contentPath = '../'

// 获取当前分支
var nowBranch = fs.readFileSync(`${contentPath}.git/HEAD`).toString().trim()
var nowBranchr = nowBranch.split("/").reverse()[0].toString()
console.log(`当前分支 ${nowBranchr}`)

// 获取当前用户
var commit = fs.readFileSync(`${contentPath}.git/logs/refs/heads/${nowBranchr}`).toString()
var currentUser = commit.slice(0, 300).split(" ")[2]
console.log(`当前用户名称 ${currentUser}`)

// 获取email
var commit = fs.readFileSync(`${contentPath}.git/logs/refs/heads/${nowBranchr}`).toString()
var currentEmail = commit.slice(0, 300).split(" ")[3].slice(1, -1)
console.log(`当前用户email ${currentEmail}`)

// 获取当前修改的commitID
var cCommitReg = new RegExp(`\\s.*?\<${currentEmail}\>`, 'g')
var commit = fs.readFileSync(`${contentPath}.git/logs/refs/heads/${nowBranchr}`).toString()
var currentCommit = commit.match(cCommitReg).reverse()[0].split(" ")[1].toString()
console.log(`当前分支commit ${currentCommit}`)

// 获取当前项目git地址
var proUrl = fs.readFileSync(`${contentPath}.git/config`).toString()

// 获取当前项目git地址项目名称
var proUrlList = proUrl.match(/(url\s?\=\s?){1}[\/|\w|\:|\.|\-]*(\.git)/g)
// console.log(proUrlList);
var currentProUrl = proUrlList[0].slice(6)
var currentProUrlName = proUrlList[0].split('/').reverse()[0].split('.')[0]
console.log(`当前项目git地址`, currentProUrl)

var commitAdd = currentCommit.slice(0, 8)
commitVal = `${currentProUrlName} ${nowBranchr} ${commitAdd} ${currentProUrl} ${commitVal}`

// 修改文件
edit.editVersion(currentProUrlName, currentProUrl, nowBranchr, currentCommit, currentUser, function () {
    shell.exec('git add .')
    shell.exec(`git commit -m "${commitVal}"`)
    shell.exec(`git pull origin master`)
    shell.exec('git push origin master')
})


