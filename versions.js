// index与config中的对应 为数组下边
let versionsJsonpList = {
    frontEnd: {
        gittest2: {
            dev: {
                name: "gitDev",
                version: "<version>V1.1.1.<name>gitDev</name></version>",
                lastBranch: "<branch>ykj</branch>",
                lastCommit: "<commit>3af84004c6bdbf9918cbacfbee99a446285deed4</commit>",
                lastUser: "<user>yankangjie</user>",
                index: 0,
            },
            pro: {
                name: "gitPro",
                version: "<version>V2.2.2.<name>gitPro</name></version>",
                lastBranch: "<branch>ykj</branch>",
                lastCommit: "<commit>3af84004c6bdbf9918cbacfbee99a446285deed4</commit>",
                lastUser: "<user>yankangjie</user>",
                index: 1,
            },
        },
        OMS: {
            dev: {
                name: "devPro",
                version: "<version>V3.9.108.<name>devPro</name></version>",
                lastBranch: "<branch>master</branch>",
                lastCommit: "<commit>e7deb9359e15006b57482e579df3e370c434f331</commit>",
                lastUser: "<user>yankangjie</user>",
                index: 2,
            },
            pro: {
                name: "bmsDev",
                version: "<version>V3.9.109.<name>bmsDev</name></version>",
                lastBranch: "<branch>ykj</branch>",
                lastCommit: "<commit>2f37ff82ea60d0267cd8142da73de55ce19b4edf</commit>",
                lastUser: "<user>yankangjie</user>",
                index: 3,
            }
        },
        BMS: {
            dev: {
                name: "bmsDev",
                version: "<version>V3.9.109.<name>bmsDev</name></version>",
                lastBranch: "<branch>V3.8.x.191023</branch>",
                lastCommit: "<commit>2e89329c734a9a3d79048f7a7a893f1a036a1bf4</commit>",
                lastUser: "<user>yankangjie</user>",
                index: 4,
            },
            pro: {
                name: "bmsPro",
                version: "<version>V3.9.146.<name>bmsPro</name></version>",
                lastBranch: "<branch>V3.8.x.191023</branch>",
                lastCommit: "<commit>0eaa9cbc3b8b5e2d8927e680c1916d8f8912dc66</commit>",
                lastUser: "<user>yankangjie</user>",
                index: 5,
            }
        }
    },

}

jsonhandle(versionsJsonpList)