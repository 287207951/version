// require('shelljs/global');
var fs = require("fs");
var argv = require('yargs').argv;
var commit = argv._[0]
var config = require('./config')

let versionName = ''

// 获取需要增加的版本号下标
function getVersionsIndex(currentProUrlName, currentProUrl, nowBranchr) {

    if (!config[currentProUrlName]) {
        console.error('请填写正确的配置文件')
    }
    if (config[currentProUrlName].url === currentProUrl) {
        return config[currentProUrlName][nowBranchr]
    }
    return false
}

function replaceBranch(versionInfo, info, index) {
    var strSource = versionInfo.match(/<branch>.*<\/branch>/g)[index]
    return versionInfo.replace(strSource, `<branch>${info}</branch>`)
}

function replaceCommit(versionInfo, info, index) {
    var strSource = versionInfo.match(/<commit>.*<\/commit>/g)[index]
    return versionInfo.replace(strSource, `<commit>${info}</commit>`)
}

function replaceUser(versionInfo, info, index) {
    var strSource = versionInfo.match(/<user>.*<\/user>/g)[index]
    return versionInfo.replace(strSource, `<user>${info}</user>`)
}

// 获取旧版本的信息
function getOldVersionInfo(versionInfo, index) {
    return versionInfo.match(/<version>.*<\/version>/g)[index]
}

// 获取版本名称
function getVersionName(options) {
    return options.match(/\<name\>.*\<\/name\>/)[0].replace('<name>', '').replace('</name>', '')
}

// 获取版本号
function getVersionNo(versionInfo, index) {

    // 提取版本号最后的数字
    function getLastStr(options) {
        return options.split('.')[2]
    }

    // 替换版本号最后一位
    function replaceLastStr(options, lastStr) {
        // console.log(options, lastStr)
        var arr = options.split('.')
        var lastIndex = arr.lastIndexOf(lastStr)
        arr.splice(lastIndex, 1, lastStr - 0 + 1)
        return arr.join('.')
    }

    // 获取版本文件
    var strSource = getOldVersionInfo(versionInfo, index)
    var str = strSource.replace('<version>', '').replace('</version>', '')

    // 获取最后一位的版本号
    var lastStr = getLastStr(str)

    // 获取版本名称
    versionName = getVersionName(str)

    // 获取完整版本号
    var repStr = replaceLastStr(str, lastStr)

    return repStr
}

function replaceVersion(versionInfo, index) {
    var versionNo = getVersionNo(versionInfo, index)
    var strSource = getOldVersionInfo(versionInfo, index)

    // 替换文件
    return versionInfo.replace(strSource, `<version>${versionNo}</version>`)
}

function editVersion(currentProUrlName, currentProUrl, nowBranchr, currentCommit, currentUser, callBack) {

    var versionIndex = getVersionsIndex(currentProUrlName, currentProUrl, nowBranchr)
    console.log(`更改版本的下标`, versionIndex)

    // 获取版本文件信息
    var versionInfo = fs.readFileSync('./versions.js').toString()

    // 获取版本号
    var versionNo = getVersionNo(versionInfo, versionIndex)

    // 替换版本号
    var versionInfoTxt = replaceVersion(versionInfo, versionIndex)

    // 替换commit
    versionInfoTxt = replaceCommit(versionInfoTxt, currentCommit, versionIndex)

    // // 替换分支
    versionInfoTxt = replaceBranch(versionInfoTxt, nowBranchr, versionIndex)

    // // 替换用户
    versionInfoTxt = replaceUser(versionInfoTxt, currentUser, versionIndex)


    // 写入文件
    fs.writeFile('versions.js', versionInfoTxt, function (err) {
        if (err) {
            return console.error(err);
        }
        callBack()
        console.log(`版本更新成功！${currentProUrlName} ${versionNo}`);
    });

}

module.exports = {
    editVersion,
}



